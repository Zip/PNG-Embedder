// No license, public domain

if(process.argv.length != 5){
    console.log("Usage: node embed [input file] [embed file] [output file]");
    process.exit(1);
}

const inputImage = process.argv[2];
const embedFile = process.argv[3];
const outputImage = process.argv[4];

const fs = require('fs');
const toUint8Array = require('./fucknode.js');
const {PNG, PNGChunk} = require('./pngchunk.js');


const pngFile = fs.readFileSync(inputImage);
const pngArrBuf = toUint8Array(pngFile);
const png = new PNG(pngArrBuf);

const contentFile = fs.readFileSync(embedFile);
const contentBuf = toUint8Array(contentFile);
const contentChunk = PNGChunk.create('IDAT', contentBuf);
png.addChunk(contentChunk);

// We'll use this to find the right IDAT chunk
const infoEncoder = new TextEncoder();
const info = JSON.stringify({
    ext: embedFile.split('.').pop(),
    size: contentChunk.length
});
const typeChunk = PNGChunk.create('chAn', infoEncoder.encode(info));
png.addChunk(typeChunk);

console.log(png);

const exportData = png.export();
fs.writeFileSync(outputImage, exportData);

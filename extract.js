// No license, public domain

if(process.argv.length != 4){
    console.log("Usage: node extract [input file] [output file]");
    process.exit(1);
}

const inputImage = process.argv[2];
const outputImage = process.argv[3];

const fs = require('fs');
const toUint8Array = require('./fucknode.js');
const {PNG} = require('./pngchunk.js');


const pngFile = fs.readFileSync(inputImage);
const pngArrBuf = toUint8Array(pngFile);
const png = new PNG(pngArrBuf);

let size = 0;
let ext;
const infoDecoder = new TextDecoder();
for(let chunk of png.chunks){
    if(chunk.type != 'chAn') continue;
    try{
        let decodedText = infoDecoder.decode(chunk.data);
        let parsed = JSON.parse(decodedText);
        size = parsed.size;
        ext = parsed.ext;
    }catch(err){
        continue;
    }
}
if(size == 0) throw new Error("Couldn't find a chAn chunk");

let data;
for(let i = png.chunks.length - 1; i >= 0; i--){
    if(png.chunks[i].type = 'IDAT' && png.chunks[i].length == size) data = png.chunks[i].data;
}
if(data == undefined) throw new Error("Couldn't find IDAT chunk with right size");

fs.writeFileSync(outputImage, data);

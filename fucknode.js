// No license, public domain

/**
 * Converts NodeJS Buffer to Uint8Array
 * @param {Buffer}       buf Buffer to convert to Uint8Array
 * @returns {Uint8Array}
 */
function toUint8Array(buf) {
    const view = new Uint8Array(buf.length);
    for (let i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return view;
}

module.exports = toUint8Array;
# PNG Embedder
Embed arbitrary data in PNG images and view it with a userscript.

## Installation
The userscript for 4chan can be installed by clicking [here](https://codeberg.org/Zip/PNG-Embedder/raw/branch/master/pngchunkloader.user.js).

## Embedding
There's currently no graphical way of embedding. It has do be done from the command line with NodeJS. No NPM packages are required.

    node embed [input file] [embed file] [output file]

## Viewing
If there's a PNG in a post, there will be a link button labeled `PNG` to the right of the image size and resolution. Clicking it will show the embedded file. If there is no file embedded in the PNG, then the button will become strikethrough.

## To do
- Add embedding tool to userscript

## License
Parts of this project are licensed under GPL-3.0, LGPL-3.0 or public domain. Refer the notice at the start of each source file for its license.
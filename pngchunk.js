// PNG Chunk, a JavaScript library for reading and writing PNG chunks.
// Copyright (C) 2021  Zipdox
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


const CRC32 = (function(){
    var table = new Uint32Array(256);
    for(var i=256; i--;){
        var tmp = i;

        for(var k=8; k--;){
            tmp = tmp & 1 ? 3988292384 ^ tmp >>> 1 : tmp >>> 1;
        }

        table[i] = tmp;
    }

    /**
     * Calculates CRC-32
     * @param {Uint8Array}   data Text byte length limit
     * @returns {Uint8Array} Return of the CRC
     */
    return function(data){
        var crc = -1;

        for(var i=0, l = data.length; i < l; i++){
            crc = crc >>> 8 ^ table[ crc & 255 ^ data[i] ];
        }

        crc = (crc ^ -1) >>> 0;
        return Uint8Array.from([crc >> 24 & 0xFF, crc >> 16 & 0xFF, crc >> 8 & 0xFF, crc & 0xFF]);
    };
})();

/**
 * Compares two CRC-32s
 * @param {Uint8Array} crc1
 * @param {Uint8Array} crc2
 * @returns {boolean}  Whether the CRCs are the same
 */
function compareCRC32(crc1, crc2){
    for(let i = 0; i < 4; i++){
        if(crc1[i] != crc2[i]) return false;
    }
    return true;
}

class PNGChunk{
    /**
     * Parse a PNG Chunk
     * @param {Uint8Array} array The chunk content (size, type, data and CRC);
     */
    constructor(array){
        this.array = array;

        this.length = PNGChunk.testLength(array, 0);

        const decoder = new TextDecoder();
        this.typeArrray = array.subarray(4, 8);
        this.type = decoder.decode(this.typeArrray);

        this.data = array.subarray(8, 8 + this.length);

        this.crc = array.subarray(8 + this.length, 8 + this.length + 4);

        this.crcCorrect = compareCRC32(CRC32(array.subarray(4, 8 + this.length)), this.crc);
    }

    /**
     * Tests the length of a PNG chunk
     * @param {Uint8Array} array The array in which the chunk exists
     * @param {number}     index The index in the array at which the chunk starts
     * @returns {number}   Length of the chunk data
     */
    static testLength(array, index){
        const lengthArray = array.subarray(index, index + 4);
        return (lengthArray[0] << 24) | (lengthArray[1] << 16) | (lengthArray[2] << 8) | lengthArray[3];
    }

    /**
     * Creates a new PNG chunk, a copy of the buffer will be created
     * @param {string}     type The type of the chunk
     * @param {Uint8Array} data The data to put in the chunk
     * @returns {PNGChunk} The created PNG chunk
     */
    static create(type, data){
        const encoder = new TextEncoder()
        const typeArray = encoder.encode(type);
        if(typeArray.length != 4) return null;

        const chunkArray = new Uint8Array(data.length + 12);

        chunkArray[0] = data.length >> 24 & 0xFF;
        chunkArray[1] = data.length >> 16 & 0xFF;
        chunkArray[2] = data.length >> 8 & 0xFF;
        chunkArray[3] = data.length & 0xFF;

        chunkArray.set(typeArray, 4);

        chunkArray.set(data, 8);

        const crc = CRC32(chunkArray.subarray(4, 8 + data.length));
        chunkArray.set(crc, 8 + data.length);

        return new PNGChunk(chunkArray);
    }
}

class PNG{
    /**
     * Create a PNG, a copy of the buffer will be created
     * @param {Uint8Array} array Data to load the PNG from
     */
    constructor(array){
        this.header = new Uint8Array([0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]);
        this.initialized = true;
        for(let i = 0; i < this.header.length; i++){
            if(array[i] != this.header[i]){
                this.initialized = false;
                return;
            }
        }
        
        this.chunks = [];
        for(let i = 8; i < array.length;){
            let chunkLength = PNGChunk.testLength(array, i) + 12;
            let chunk = new PNGChunk(array.slice(i, i + chunkLength));
            if(!chunk.crcCorrect){
                throw new Error('CRC incorrect');
            }
            if(chunk.type == 'IEND'){
                this.endChunk = chunk;
            }else{
                this.chunks.push(chunk);
            }
            i += chunkLength;
        }
    }

    /**
     * Add a chunk to the PNG, a copy of the buffer will be created
     * @param {PNGChunk} data The PNG chunk
     */
    addChunk(chunk){
        this.chunks.push(chunk);
    }

    /**
     * Exports the PNG file
     * @returns {Uint8Array} The exported PNG file
     */
    export(){
        let totalLength = this.header.length + this.endChunk.array.length;
        for(let chunk of this.chunks) totalLength += chunk.array.length;

        const result = new Uint8Array(totalLength);

        let pos = 0;
        result.set(this.header, 0);
        pos = this.header.length;

        for(let i = 0; i < this.chunks.length; i++){
            result.set(this.chunks[i].array, pos);
            pos += this.chunks[i].array.length;
        }

        result.set(this.endChunk.array, pos);

        return result;
    }
}

module.exports = {PNGChunk, PNG};
